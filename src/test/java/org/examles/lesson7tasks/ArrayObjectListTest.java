package org.examles.lesson7tasks;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import  static  org.junit.jupiter.api.Assertions.*;

class ArrayObjectListTest {
    @Test
    @DisplayName("проверка предикатов")
    void predicateTest() {
        ArrayObjectList<Integer> arr = new ArrayObjectList<>();
        arr.add(5);
        arr.add(10);
        arr.add(15);

        assertTrue(arr.every((a)->a>0));
        assertFalse(arr.every((a)->a>5));
        assertTrue(arr.some((a)->a>10));
        assertFalse(arr.some((a)->a>20));
    }
    @Test
    @DisplayName("проверка reduce")
    void reduceTest() {
        ArrayObjectList<Integer> arr = new ArrayObjectList<>();
        arr.add(5);
        arr.add(10);
        arr.add(15);
        arr.add(3);

       //итератор реверсивный, поэтому элементы перебираются с конца
       assertEquals(33,arr.reduce(3,(c,p)->c+p));
       assertEquals(2250,arr.reduce(3,(c,p)->c*p));

    }


}