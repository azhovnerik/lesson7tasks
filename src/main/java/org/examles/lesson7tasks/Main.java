package org.examles.lesson7tasks;

import java.util.Iterator;


public class Main {
    public static void main(String[] args) {

        ArrayObjectList<String>  names = new ArrayObjectList<String>();
        names.add("petya");
        names.add("vasya");
        names.add("gena");

        //inversive order
        Iterator<String> iterator =  ((ArrayObjectList<String>) names).iterator();

        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }

        for(String i:names){
            System.out.println("name:"+i);
        }



    }
}
