package org.examles.lesson7tasks;

public interface ObjectList<T> {
    T get(int index);
    void add(T value);
    void addAll(ObjectList<T> list);
    void set(T value, int index);
    int size();

}
