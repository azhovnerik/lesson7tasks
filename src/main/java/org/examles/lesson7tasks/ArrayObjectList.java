package org.examles.lesson7tasks;

import java.util.Iterator;
import java.util.function.BiFunction;
import java.util.function.Predicate;

public class ArrayObjectList<T> implements ObjectList<T>,Iterable<T> {
    private final static int DEFAULT_CAPACITY = 15;
    private static final double CAPACITY_SCALE = 1.5;
    Object[] objects;

    int size =0;


    public ArrayObjectList(int capacity) {
       // this.CAPACITY = CAPACITY;
        objects = new Object[capacity];
    }

    public ArrayObjectList() {
        objects = new Object[DEFAULT_CAPACITY];
    }

    //класс итератора
     public static class MyIterator<T> implements Iterator<T> {
        private Object[] arr;
        private int index=0;
        private  int size;


        public MyIterator(Object[] arr,int size) {
            this.arr = arr;
            index=0;
            this.size = size;
        }

        @Override
        public boolean hasNext() {
            //return index<arr.length;
            return index < size;
        }

        @Override
        public T next() {
            return (T)arr[index++];
        }
    }
    //класс итератора инверсный
    //public static class MyIteratorInverse<T> implements Iterator<T> {
    private   class MyIteratorInverse<T> implements Iterator<T> {
       // private Object[] arr;
        private int index;
       // private  int size;


        public MyIteratorInverse() {
            //this.arr = arr;
            this.index= size-1;
           // this.size = size;
        }

        @Override
        public boolean hasNext() {
            //return index<arr.length;
            return index >= 0;
        }

        @Override
        public T next() {
            return (T)objects[index--];
        }
    }



    //@Override
    //public Iterator<T> iterator() {
    //    return new MyIteratorInverse<>();
    //}

    //реализуем итератор в анонимном классе
    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {
            private int index = size-1;

            @Override
            public boolean hasNext() {
                return index >= 0;
            }

            @Override
            public T next() {
                return (T)objects[index--];
            }
        };
    }


    @Override
    public T get(int index) {
        checkIndex(index);
        return (T) objects[index];
    }

    private void checkIndex(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException("индекс " + index + " за пределами массива");
        }
    }

    @Override
    public void add(T value) {
        if (objects.length > size) {
            objects[size++]=value;
        }
        else {
            expand();
            objects[size++] = value;

        }
    }


    private void expand() {
        int newCapacity = (int) (objects.length*CAPACITY_SCALE + 1);
        Object[] buffer = new Object[newCapacity];
        for (int i = 0; i < objects.length; i++) {
            buffer[i] = objects[i];

        }
        objects = buffer;
    }

    @Override
    public void addAll(ObjectList<T> list) {

        for (int i = 0; i < list.size(); i++) {
            add(list.get(i));

        }

    }
    @Override
    public void set(T value, int index) {
        checkIndex(index);
        objects[index]=value;
    }
    @Override
    public int size() {
       return  size ;
    }

    //Предикат every
    public boolean every(Predicate<T> predicate){
        boolean result = true;
        for (T t: this){
            if (!predicate.test(t)){
                result=false;
                break;
            }
        }
        return result;

        //Предикат some
    } public boolean some(Predicate<T> predicate){
        boolean result = false;
        for (T t: this){
            if (predicate.test(t)){
                result=true;
                break;
            }
        }
        return result;
    }
   public <R> R  reduce(R initValue, BiFunction<T,R,R> f) {
       R curResult = null;
       int previousResult;
       boolean first = true;
       for (T t : this) {
           if (first) {
               curResult = initValue;
               first = false;
           } else {
               curResult = f.apply(t, curResult);
           }

       }
       return curResult;
   }


}
